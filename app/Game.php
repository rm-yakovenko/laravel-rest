<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = ['title'];

    protected $hidden = ['deleted_at'];
}
